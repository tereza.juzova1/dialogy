# Dialogy
Welcome... to the pages. :ghost:


## Popis projektu

Jedná se o vypracování úkolu k výběrovému řízení na pozici. **Manažer datových analytiků pro divizi Vyhledávaní**. 

Poskytunuté soubory:

- dialogy1.txt
- dialogy2.txt
- modifikatory.txt
- zadani.txt

## Použité nástroje
V tomto projektu jsem využila Apache Spark, konkrétně jsem použila jeho Python rozhraní - PySpark. PySpark umožňuje efektivní zpracování velkých datových souborů distribuovaně pomocí klíčové struktury RDD (Resilient Distributed Datasets) a poskytuje nám také jednoduché API pro tvorbu paralelních datových toků.

```
- Apache Spark: verze 2.4.7
- PySpark: verze 2.4.7
- Python: verze 3.7.12
- Numpy: verze 2.2.1
- Matplotlib: verze 3.3.4
- Spark NLP: verze 5.0.1
```

## Struktura repozitáže
```
dialogy/
├── docs/
│   ├── img/
│   │   └── pocet_dotazu_dialogy1.png
│   ├── index.md
│   └── mkdocs_reseni_a.md
│   └── mkdocs_reseni_b.md
│   └── mkdocs_reseni_c.md
│   └── mkdocs_reseni_d.md
│   └── mkdocs_reseni_e.md
│   └── mkdocs_reseni_f.md
│   └── mkdocs_reseni_g.md
│   └── mkdocs_zadani.md
├── input_data/
│   ├── data/
│   │   └── dialogy1.txt
│   │   └── dialogy2.txt
│   ├── test/
│   │   └── dialogy_test.txt
│   └── modifikatory.txt
└── task/
│   ├── zadani.md
│   ├── zadani.txt
└── task_development/
│   ├── notebooks/
│   │   └── modifikatory_v_dialozich.ipynb
│   │   └── pocet_dotazu_ve_stejnem_casovem_useku.ipynb
│   │   └── pocet_vsech_dialogu.ipynb
│   │   └── predzpracovani_dat.ipynb
│   └── reseni_a.md
│   └── reseni_b.md
│   └── reseni_c.md
│   └── reseni_d.md
│   └── reseni_e.md
│   └── reseni_f.md
│   └── reseni_g.md
├── .gitlab-ci-yml
├── Dockerfile
├── README.md
├── mkdocs.yml

```

## Pages
Pro přehlednost je použitá speciální funkce Pages, která umožňuje jednoduché publikování webových stránek přímo z GitLab repozitáře.

Zde je odkaz na [statické stránky](https://dialogy-tereza-juzova1-62117d03268e06233909e2284d127df51cdc1631.gitlab.io).

Je použita knihovna [MkDocs](https://www.mkdocs.org/) k tvorbě statické dokumentace webových stránek z formátovaného textu - makrdown.

V Pages se nachází jednoduchá struktura s popisem projektu, popis [zadání](https://dialogy-tereza-juzova1-62117d03268e06233909e2284d127df51cdc1631.gitlab.io/mkdocs_zadani/) a [řešení](https://dialogy-tereza-juzova1-62117d03268e06233909e2284d127df51cdc1631.gitlab.io/mkdocs_reseni/).

## Vypracování úkolu

V Pages v záložce řešení se nachází vypracování úkolu, jsou zde poskytnuty odpovědi na uvedené otázky. 

```
dialogy/
└── task_development/
│   ├── notebooks/
│   │   └── modifikatory_v_dialozich.ipynb
│   │   └── pocet_dotazu_ve_stejnem_casovem_useku.ipynb
│   │   └── pocet_vsech_dialogu.ipynb
│   │   └── predzpracovani_dat.ipynb
│   └── reseni_a.md
│   └── reseni_b.md
│   └── reseni_c.md
│   └── reseni_d.md
│   └── reseni_e.md
│   └── reseni_f.md
│   └── reseni_g.md
```
Některé odpovědi se odkazují také na příslušné notebooky, je vždy uvedeno v textu.

Vstupní a testovací data se nachází v repozitáři zde:

```
dialogy/
├── input_data/
│   ├── data/
│   │   └── dialogy1.txt
│   │   └── dialogy2.txt
│   ├── test/
│   │   └── dialogy_test.txt
│   └── modifikatory.txt
```

## Přidání souborů

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/tereza.juzova1/dialogy.git
git branch -M main
git push -uf origin main
```