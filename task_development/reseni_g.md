# Co dalšího můžete o dodaných datech říci?
## Řešení

Například mě zajímalo jaké modifikátory se nachází v dialozích. Použila jsem poskytunuté soubory **dialogy1.txt** a **modifikatory.txt** a podívala jsem se na to, kolik modifikátorů se nachází v zadaném textu a jaké modifikátory se tam nachází, dále pak jestli text/dialog má v sobě více než jeden modifikátor a také jaké texty nemají žádné modifikátory. Uvažovala jsem hledaní v přesné shodě.

**Pozn.: vycházím z notebooku - [modifikatory_v_dialozich.ipynb](https://gitlab.com/tereza.juzova1/dialogy/-/blob/main/task_development/notebooks/modifikatory_v_dialozich.ipynb?ref_type=heads)**

Použila jsem dva trochu odlišné kódy, oba dva mají stejný cíl: najít a vypočítat modifikátory v textovém sloupci. Nicméně, použila jsem dva odlišné přístupy a funkce:

## **První kód**

- Využívá metodu in a seznamové porovnání k nalezení modifikátorů v textu. Porovnává, zda je každý modifikátor získaný ze souboru (proměnná modifikatory) obsažen v textovém sloupci DataFrame (sloupec text). Používá se metoda strip() k odstranění případných bílých znaků na začátku a konci řádku ze souboru s modifikátory.
- Používá funkci udf (User Defined Function) pro aplikaci vlastní funkce najdi_modifikatory na DataFrame. Tato funkce vrací seznam nalezených modifikátorů.
- Přidá sloupec "modifikatory" do DataFrame, který obsahuje nalezené modifikátory v textu.
- Používá další UDF (pocet_modifikatoru) k počítání počtu modifikátorů v textu a přidá sloupec "pocet_modifikatoru_v_dialogu" do DataFrame.

### Výstup
```
+------------------+-----+-----------------------------+----------------------+----------------------------+
|dialog            |time |text                         |modifikatory          |pocet_modifikatoru_v_dialogu|
+------------------+-----+-----------------------------+----------------------+----------------------------+
|<dialog #1 (2299)>|07:19|boty do vody                 |[boty]                |1                           |
|<dialog #1 (2299)>|07:19|boty do vody k moři          |[boty]                |1                           |
|<dialog #1 (2299)>|07:19|boty do vody pro děti        |[boty]                |1                           |
|<dialog #1 (2299)>|07:20|boty na beh                  |[boty]                |1                           |
|<dialog #1 (2299)>|07:20|boty na běh                  |[boty]                |1                           |
|<dialog #1 (2299)>|07:20|boty na bezky                |[boty]                |1                           |
|<dialog #1 (2299)>|07:20|boty na běžky                |[boty]                |1                           |
|<dialog #1 (2299)>|07:20|boty na běžky výprodej       |[boty, výprodej]      |2                           |
|<dialog #1 (2299)>|07:20|boty na házenou              |[boty]                |1                           |
|<dialog #1 (2299)>|07:20|boty na nohejbal             |[boty]                |1                           |
|<dialog #1 (2299)>|07:20|boty na nohejbal botas       |[boty]                |1                           |
|<dialog #1 (2299)>|07:20|boty na orientační běh       |[boty]                |1                           |
|<dialog #1 (2299)>|07:20|box fit na rukavice          |[box, rukavice]       |2                           |
|<dialog #1 (2299)>|07:20|box na rukavice              |[box, rukavice]       |2                           |
|<dialog #1 (2299)>|07:20|boxerske boty                |[boty]                |1                           |
|<dialog #1 (2299)>|07:20|boxerské boty                |[boty]                |1                           |
|<dialog #1 (2299)>|07:20|boxerské pytle               |[]                    |0                           |
|<dialog #1 (2299)>|07:20|boxerský pytel               |[]                    |0                           |
|<dialog #1 (2299)>|07:21|boxovaci pytel               |[]                    |0                           |
|<dialog #1 (2299)>|07:21|boxovací pytel               |[]                    |0                           |
|<dialog #1 (2299)>|07:21|boxovací pytel pro děti      |[]                    |0                           |
|<dialog #1 (2299)>|07:21|boxovací pytel se stojanem   |[]                    |0                           |
|<dialog #1 (2299)>|07:21|boxovaci pytle               |[]                    |0                           |
|<dialog #1 (2299)>|07:21|boxovací pytle               |[]                    |0                           |
|<dialog #1 (2299)>|07:21|boxovací rukavice na thai box|[box, rukavice]       |2                           |
|<dialog #1 (2299)>|07:21|brankářská hokejová výstroj  |[]                    |0                           |
|<dialog #1 (2299)>|07:21|brankářské dresy             |[]                    |0                           |
|<dialog #1 (2299)>|07:21|brankářský dres              |[]                    |0                           |
|<dialog #1 (2299)>|07:21|branky na florbal            |[florbal]             |1                           |
|<dialog #1 (2299)>|07:21|branky na házenou            |[]                    |0                           |
|<dialog #1 (2299)>|07:21|branky na kroket             |[]                    |0                           |
|<dialog #1 (2299)>|07:21|brasna na kolo               |[na kolo, kolo]       |2                           |
|<dialog #1 (2299)>|07:21|brasny na kolo               |[na kolo, kolo]       |2                           |
|<dialog #1 (2299)>|07:22|brašna na kolo               |[na kolo, kolo]       |2                           |
|<dialog #1 (2299)>|07:22|brašna na kolo na řidítka    |[na kolo, kolo]       |2                           |
|<dialog #1 (2299)>|07:22|brašna na nosič kola         |[kola]                |1                           |
|<dialog #1 (2299)>|07:22|brašna na rám kola           |[kola]                |1                           |
|<dialog #1 (2299)>|07:22|brašny na kola na nosiče     |[kola]                |1                           |
|<dialog #1 (2299)>|07:22|brašny na kolo               |[na kolo, kolo]       |2                           |
|<dialog #1 (2299)>|07:22|brusle 2v1                   |[]                    |0                           |
|<dialog #1 (2299)>|07:22|brusle kolečkové             |[]                    |0                           |
|<dialog #1 (2299)>|07:22|brýle na běžky               |[brýle]               |1                           |
|<dialog #1 (2299)>|07:22|brzdící padák                |[]                    |0                           |
|<dialog #1 (2299)>|07:22|bunda na kolo                |[na kolo, kolo, bunda]|3                           |
|<dialog #1 (2299)>|07:22|bundy na kolo                |[na kolo, kolo]       |2                           |
|<dialog #1 (2299)>|07:22|buzola                       |[]                    |0                           |
|<dialog #1 (2299)>|07:22|buzoly                       |[]                    |0                           |
|<dialog #1 (2299)>|07:22|camping křeslo               |[]                    |0                           |
|<dialog #1 (2299)>|07:23|camping nábytek              |[nábytek]             |1                           |
|<dialog #1 (2299)>|07:23|camping outdoor              |[]                    |0                           |
|<dialog #1 (2299)>|07:23|camping stolek               |[]                    |0                           |
|<dialog #1 (2299)>|07:23|camping židle                |[]                    |0                           |
|<dialog #1 (2299)>|07:23|campingové nádobí            |[]                    |0                           |
|<dialog #1 (2299)>|07:23|campingové stoly             |[]                    |0                           |
|<dialog #1 (2299)>|07:23|campingové židle             |[]                    |0                           |
|<dialog #1 (2299)>|07:23|cestovní bag golf            |[golf]                |1                           |
|<dialog #1 (2299)>|07:23|cinka                        |[]                    |0                           |
|<dialog #1 (2299)>|07:23|cinky                        |[]                    |0                           |
|<dialog #1 (2299)>|07:23|cinky 3 kg                   |[]                    |0                           |
|<dialog #1 (2299)>|07:23|cinky jednorucky             |[]                    |0                           |
|<dialog #1 (2299)>|07:23|cinky na posilovani          |[]                    |0                           |
|<dialog #1 (2299)>|07:23|cinky sada                   |[sada]                |1                           |
|<dialog #1 (2299)>|07:23|cvicebni kruhy hula hoop     |[]                    |0                           |
|<dialog #1 (2299)>|07:24|cvičební guma posilovací     |[]                    |0                           |
|<dialog #1 (2299)>|07:24|cvičební gumy posilovací     |[]                    |0                           |
|<dialog #1 (2299)>|07:24|cvičení na balanční podložce |[]                    |0                           |
|<dialog #1 (2299)>|07:24|cvičení s posilovací gumou   |[]                    |0                           |
|<dialog #1 (2299)>|07:24|cvičky na balet              |[]                    |0                           |
|<dialog #1 (2299)>|07:24|cviky s činkami              |[cviky]               |1                           |
|<dialog #1 (2299)>|07:24|cyklistická bunda            |[bunda]               |1                           |
|<dialog #1 (2299)>|07:24|cyklisticka helma            |[helma]               |1                           |
|<dialog #1 (2299)>|07:24|cyklistická helma            |[helma]               |1                           |
|<dialog #1 (2299)>|07:24|cyklistická přilba           |[]                    |0                           |
|<dialog #1 (2299)>|07:24|cyklistická vesta            |[]                    |0                           |
|<dialog #1 (2299)>|07:24|cyklisticke bundy            |[]                    |0                           |
|<dialog #1 (2299)>|07:24|cyklistické bundy            |[]                    |0                           |
|<dialog #1 (2299)>|07:24|cyklisticke dresy            |[]                    |0                           |
|<dialog #1 (2299)>|07:24|cyklistické dresy            |[]                    |0                           |
|<dialog #1 (2299)>|07:25|cyklistické dresy a trička   |[]                    |0                           |
|<dialog #1 (2299)>|07:25|cyklistické dresy dámské     |[dámské]              |1                           |
|<dialog #1 (2299)>|07:25|cyklisticke helmy            |[]                    |0                           |
|<dialog #1 (2299)>|07:25|cyklistické helmy            |[]                    |0                           |
|<dialog #1 (2299)>|07:25|cyklistické helmy dětské     |[]                    |0                           |
|<dialog #1 (2299)>|07:25|cyklisticke kalhoty          |[kalhoty]             |1                           |
|<dialog #1 (2299)>|07:25|cyklistické kalhoty          |[kalhoty]             |1                           |
|<dialog #1 (2299)>|07:25|cyklistické kalhoty dlouhé   |[kalhoty]             |1                           |
|<dialog #1 (2299)>|07:25|cyklistické přilby           |[]                    |0                           |
|<dialog #1 (2299)>|07:25|cyklisticke rukavice         |[rukavice]            |1                           |
|<dialog #1 (2299)>|07:25|cyklistické rukavice         |[rukavice]            |1                           |
|<dialog #1 (2299)>|07:25|cyklistické rukavice zimní   |[rukavice]            |1                           |
|<dialog #1 (2299)>|07:25|cyklistické tričko           |[]                    |0                           |
|<dialog #1 (2299)>|07:25|cyklistické vesty            |[]                    |0                           |
|<dialog #1 (2299)>|07:25|cyklisticky dres             |[]                    |0                           |
|<dialog #1 (2299)>|07:26|cyklistický dres             |[]                    |0                           |
|<dialog #1 (2299)>|07:26|cyklistický dres dámský      |[]                    |0                           |
|<dialog #1 (2299)>|07:26|cyklistický dres dětský      |[]                    |0                           |
|<dialog #1 (2299)>|07:26|cyklo brašna                 |[cyklo]               |1                           |
|<dialog #1 (2299)>|07:26|cyklo brašna na zadní nosič  |[cyklo]               |1                           |
|<dialog #1 (2299)>|07:26|cyklo brašny                 |[cyklo]               |1                           |
|<dialog #1 (2299)>|07:26|cyklo brašny na zadní nosič  |[cyklo]               |1                           |
+------------------+-----+-----------------------------+----------------------+----------------------------+
only showing top 100 rows
```

## Druhý kód
- Používá moduly re (regular expressions), split a array pro nalezení modifikátorů v textu. Využívá regulární výrazy k hledání celých slov (word boundaries), což umožňuje hledat přesné shody slov v textu, nezávisle na velikosti písmen (ignore case).
- Proměnná modifikatory_list obsahuje seznam modifikátorů načtených ze souboru.
- Definuje funkci najdi_modifikatory, která používá regulární výrazy pro hledání modifikátorů v textu. Tato funkce vrátí nalezené modifikátory.
- Používá funkci udf pro aplikaci vlastní funkce najdi_modifikatory na DataFrame. Tato funkce vrací seznam nalezených modifikátorů.
- Přidá sloupec "modifikatory" do DataFrame, který obsahuje nalezené modifikátory v textu.
- Používá funkci size (ze třídy pyspark.sql.functions) pro spočítání počtu modifikátorů v textu a přidá sloupec "pocet_modifikatoru_v_dialogu" do DataFrame.

### Výstup
```
+------------------+-----+-----------------------------+----------------------+----------------------------+
|dialog            |time |text                         |modifikatory          |pocet_modifikatoru_v_dialogu|
+------------------+-----+-----------------------------+----------------------+----------------------------+
|<dialog #1 (2299)>|07:19|boty do vody                 |[boty]                |1                           |
|<dialog #1 (2299)>|07:19|boty do vody k moři          |[boty]                |1                           |
|<dialog #1 (2299)>|07:19|boty do vody pro děti        |[pro děti, boty, děti]|3                           |
|<dialog #1 (2299)>|07:20|boty na beh                  |[boty]                |1                           |
|<dialog #1 (2299)>|07:20|boty na běh                  |[boty, běh]           |2                           |
|<dialog #1 (2299)>|07:20|boty na bezky                |[boty]                |1                           |
|<dialog #1 (2299)>|07:20|boty na běžky                |[boty]                |1                           |
|<dialog #1 (2299)>|07:20|boty na běžky výprodej       |[boty, výprodej]      |2                           |
|<dialog #1 (2299)>|07:20|boty na házenou              |[boty]                |1                           |
|<dialog #1 (2299)>|07:20|boty na nohejbal             |[boty]                |1                           |
|<dialog #1 (2299)>|07:20|boty na nohejbal botas       |[boty]                |1                           |
|<dialog #1 (2299)>|07:20|boty na orientační běh       |[boty, běh]           |2                           |
|<dialog #1 (2299)>|07:20|box fit na rukavice          |[box, rukavice]       |2                           |
|<dialog #1 (2299)>|07:20|box na rukavice              |[box, rukavice]       |2                           |
|<dialog #1 (2299)>|07:20|boxerske boty                |[boty]                |1                           |
|<dialog #1 (2299)>|07:20|boxerské boty                |[boty]                |1                           |
|<dialog #1 (2299)>|07:20|boxerské pytle               |[]                    |0                           |
|<dialog #1 (2299)>|07:20|boxerský pytel               |[]                    |0                           |
|<dialog #1 (2299)>|07:21|boxovaci pytel               |[]                    |0                           |
|<dialog #1 (2299)>|07:21|boxovací pytel               |[]                    |0                           |
|<dialog #1 (2299)>|07:21|boxovací pytel pro děti      |[pro děti, děti]      |2                           |
|<dialog #1 (2299)>|07:21|boxovací pytel se stojanem   |[]                    |0                           |
|<dialog #1 (2299)>|07:21|boxovaci pytle               |[]                    |0                           |
|<dialog #1 (2299)>|07:21|boxovací pytle               |[]                    |0                           |
|<dialog #1 (2299)>|07:21|boxovací rukavice na thai box|[box, rukavice]       |2                           |
|<dialog #1 (2299)>|07:21|brankářská hokejová výstroj  |[]                    |0                           |
|<dialog #1 (2299)>|07:21|brankářské dresy             |[]                    |0                           |
|<dialog #1 (2299)>|07:21|brankářský dres              |[]                    |0                           |
|<dialog #1 (2299)>|07:21|branky na florbal            |[florbal]             |1                           |
|<dialog #1 (2299)>|07:21|branky na házenou            |[]                    |0                           |
|<dialog #1 (2299)>|07:21|branky na kroket             |[]                    |0                           |
|<dialog #1 (2299)>|07:21|brasna na kolo               |[na kolo, kolo]       |2                           |
|<dialog #1 (2299)>|07:21|brasny na kolo               |[na kolo, kolo]       |2                           |
|<dialog #1 (2299)>|07:22|brašna na kolo               |[na kolo, kolo]       |2                           |
|<dialog #1 (2299)>|07:22|brašna na kolo na řidítka    |[na kolo, kolo]       |2                           |
|<dialog #1 (2299)>|07:22|brašna na nosič kola         |[kola]                |1                           |
|<dialog #1 (2299)>|07:22|brašna na rám kola           |[kola]                |1                           |
|<dialog #1 (2299)>|07:22|brašny na kola na nosiče     |[kola]                |1                           |
|<dialog #1 (2299)>|07:22|brašny na kolo               |[na kolo, kolo]       |2                           |
|<dialog #1 (2299)>|07:22|brusle 2v1                   |[]                    |0                           |
|<dialog #1 (2299)>|07:22|brusle kolečkové             |[]                    |0                           |
|<dialog #1 (2299)>|07:22|brýle na běžky               |[brýle]               |1                           |
|<dialog #1 (2299)>|07:22|brzdící padák                |[]                    |0                           |
|<dialog #1 (2299)>|07:22|bunda na kolo                |[na kolo, kolo, bunda]|3                           |
|<dialog #1 (2299)>|07:22|bundy na kolo                |[na kolo, kolo]       |2                           |
|<dialog #1 (2299)>|07:22|buzola                       |[]                    |0                           |
|<dialog #1 (2299)>|07:22|buzoly                       |[]                    |0                           |
|<dialog #1 (2299)>|07:22|camping křeslo               |[]                    |0                           |
|<dialog #1 (2299)>|07:23|camping nábytek              |[nábytek]             |1                           |
|<dialog #1 (2299)>|07:23|camping outdoor              |[]                    |0                           |
|<dialog #1 (2299)>|07:23|camping stolek               |[]                    |0                           |
|<dialog #1 (2299)>|07:23|camping židle                |[židle]               |1                           |
|<dialog #1 (2299)>|07:23|campingové nádobí            |[]                    |0                           |
|<dialog #1 (2299)>|07:23|campingové stoly             |[]                    |0                           |
|<dialog #1 (2299)>|07:23|campingové židle             |[židle]               |1                           |
|<dialog #1 (2299)>|07:23|cestovní bag golf            |[golf]                |1                           |
|<dialog #1 (2299)>|07:23|cinka                        |[]                    |0                           |
|<dialog #1 (2299)>|07:23|cinky                        |[]                    |0                           |
|<dialog #1 (2299)>|07:23|cinky 3 kg                   |[]                    |0                           |
|<dialog #1 (2299)>|07:23|cinky jednorucky             |[]                    |0                           |
|<dialog #1 (2299)>|07:23|cinky na posilovani          |[]                    |0                           |
|<dialog #1 (2299)>|07:23|cinky sada                   |[sada]                |1                           |
|<dialog #1 (2299)>|07:23|cvicebni kruhy hula hoop     |[]                    |0                           |
|<dialog #1 (2299)>|07:24|cvičební guma posilovací     |[]                    |0                           |
|<dialog #1 (2299)>|07:24|cvičební gumy posilovací     |[]                    |0                           |
|<dialog #1 (2299)>|07:24|cvičení na balanční podložce |[cvičení]             |1                           |
|<dialog #1 (2299)>|07:24|cvičení s posilovací gumou   |[cvičení]             |1                           |
|<dialog #1 (2299)>|07:24|cvičky na balet              |[]                    |0                           |
|<dialog #1 (2299)>|07:24|cviky s činkami              |[cviky]               |1                           |
|<dialog #1 (2299)>|07:24|cyklistická bunda            |[bunda]               |1                           |
|<dialog #1 (2299)>|07:24|cyklisticka helma            |[helma]               |1                           |
|<dialog #1 (2299)>|07:24|cyklistická helma            |[helma]               |1                           |
|<dialog #1 (2299)>|07:24|cyklistická přilba           |[]                    |0                           |
|<dialog #1 (2299)>|07:24|cyklistická vesta            |[]                    |0                           |
|<dialog #1 (2299)>|07:24|cyklisticke bundy            |[]                    |0                           |
|<dialog #1 (2299)>|07:24|cyklistické bundy            |[]                    |0                           |
|<dialog #1 (2299)>|07:24|cyklisticke dresy            |[]                    |0                           |
|<dialog #1 (2299)>|07:24|cyklistické dresy            |[]                    |0                           |
|<dialog #1 (2299)>|07:25|cyklistické dresy a trička   |[trička]              |1                           |
|<dialog #1 (2299)>|07:25|cyklistické dresy dámské     |[dámské]              |1                           |
|<dialog #1 (2299)>|07:25|cyklisticke helmy            |[]                    |0                           |
|<dialog #1 (2299)>|07:25|cyklistické helmy            |[]                    |0                           |
|<dialog #1 (2299)>|07:25|cyklistické helmy dětské     |[dětské]              |1                           |
|<dialog #1 (2299)>|07:25|cyklisticke kalhoty          |[kalhoty]             |1                           |
|<dialog #1 (2299)>|07:25|cyklistické kalhoty          |[kalhoty]             |1                           |
|<dialog #1 (2299)>|07:25|cyklistické kalhoty dlouhé   |[kalhoty]             |1                           |
|<dialog #1 (2299)>|07:25|cyklistické přilby           |[]                    |0                           |
|<dialog #1 (2299)>|07:25|cyklisticke rukavice         |[rukavice]            |1                           |
|<dialog #1 (2299)>|07:25|cyklistické rukavice         |[rukavice]            |1                           |
|<dialog #1 (2299)>|07:25|cyklistické rukavice zimní   |[rukavice]            |1                           |
|<dialog #1 (2299)>|07:25|cyklistické tričko           |[tričko]              |1                           |
|<dialog #1 (2299)>|07:25|cyklistické vesty            |[]                    |0                           |
|<dialog #1 (2299)>|07:25|cyklisticky dres             |[]                    |0                           |
|<dialog #1 (2299)>|07:26|cyklistický dres             |[]                    |0                           |
|<dialog #1 (2299)>|07:26|cyklistický dres dámský      |[]                    |0                           |
|<dialog #1 (2299)>|07:26|cyklistický dres dětský      |[]                    |0                           |
|<dialog #1 (2299)>|07:26|cyklo brašna                 |[cyklo]               |1                           |
|<dialog #1 (2299)>|07:26|cyklo brašna na zadní nosič  |[cyklo]               |1                           |
|<dialog #1 (2299)>|07:26|cyklo brašny                 |[cyklo]               |1                           |
|<dialog #1 (2299)>|07:26|cyklo brašny na zadní nosič  |[cyklo]               |1                           |
+------------------+-----+-----------------------------+----------------------+----------------------------+
only showing top 100 rows
```

Oba dva kódy by měly produkovat podobné výsledky, tedy DataFrame s nalezenými modifikátory a sloupcem, který obsahuje počet modifikátorů v daném textu. První kód používá jednodušší přístup s použitím výchozích Python funkcí, zatímco druhý kód používá silnější nástroje regulárních výrazů pro přesnější hledání modifikátorů v textu.

V našem případě je vidět, že použití regular expressions je uspěšnější, je však mnohem náročnější na provedení.

## Dotazy v časových úsecích
Dále jsem se zajímala o to, kolik dotazů bylo zadaných ve stejném časovém úseku, na grafu je použita granularita hodina. Tedy můžeme vidět vývoj v čase za celý den pro soubor dialogy1.

**Pozn.: vycházím z notebooku - [pocet_dotazu_ve_stejnem_casovem_useku.ipynb](https://gitlab.com/tereza.juzova1/dialogy/-/blob/main/task_development/notebooks/pocet_dotazu_ve_stejnem_casovem_useku.ipynb?ref_type=heads)**

Dle grafu je vidět, že největší peaky jsou v dopoledních hodinách, během oběda se to zase maličko sníží, okolo 13 h je největší přísun dotazů a poté to zase mírně klesá do večerních hodin. Další nárůst dotazů je  okolo 19 h a od půlnoci do rána je největší útlum dotazů. Tento výsledek asi není nijak prekvapivý.

![Počet dotazů dialogy1](img/pocet_dotazu_dialogy1.png)