# Porovnejte soubor dialogy1.txt a dialogy2.txt a vysvětlete, čím podle vás mohou být rozdíly mezi nimi způsobeny.
## Řešení

Na první pohled se zdá, že v souboru dialogy1 se zdá, že text, který se vyskytuje v dialogu je v různých tvarech, s diakritikou i bez diakritiky a s použitím i synonima. 

**Například:** 

- sběrný koš, sběrný vak
- boty na bezky, boty na běžky
- boxerský pytel, boxovací pytel


V souboru dialogy2 se zdá, že jsou mnohem častěji za sebou stejná slova.

**Například:**

- 13:35	modulové domy
- 13:37	modulové domy
- 13:37	modulové domy
- 13:37	modulové domy
- 13:54	modulové domy

To je pouze porovnání na první pohled, rozhodně se to nedá takto jednoznačně říct. 

V obou datasetech jsou dialogy, které jsou úplně stejné.

**Například:** 

- dialogy1.txt: dialog #37598 (15)
- dialogy2.txt: dialog #24246 (15)

Dle mého názoru je pro každý dataset použít jiný kategorizátor nebo klasifikátor. Pro každý dataset mohl být výbrán jiný model. Různé modely mají různé schopnosti a omezení, a proto mohou vykazovat odlišnou výkonnost v rámci dané klasifikační úlohy. Nebo mohly být rozdíly v tréninkových datových sadách. Pokud různé modely byly trénovýny na různých tréninkových datech, mohou mít různé informace a výkonnost vzhledem k různým aspektrům úlohy.

Pro detailní analýzu můžeme využít nástroje pro zpracování přirozeného jazyka (NLP) a textovou analýzu. můžeme identifikovat nejčastější slova, zkoumat vzory či trendy v textu apod. 
