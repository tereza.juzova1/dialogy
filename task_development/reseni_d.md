# Jak byste z dodaných dat (dialogy1.txt a dialogy2.txt) vytvořil/a tabulku modifikátorů?
## Řešení

Postupovala bych podle následujících kroků.

## Předzpracování dat: 
Nejprve je nutné provést předzpracování/očištění dat, což zahrnuje odstranění nežádoucích znaků, převedení textu na malá písmena (nebo velká, závisí na požadavcích analýzy), odstranění diakritiky, dalším krokem je tokenizace (rozdělení textu na jednotlivá slova nebo tokeny) a odstranění zbytečných slov (stopwords) atd.

**Pozn.: vycházím z notebooku - [predzpracovani_dat.ipynb](https://gitlab.com/tereza.juzova1/dialogy/-/blob/main/task_development/notebooks/predzpracovani_dat.ipynb?ref_type=heads)**

```
Pro ukázku jednotlivých kroků je dataframe upraven pouze na pár řádků.
```
```
Převod textu na malá písmena a odstranění diaktritiky --> vysledný sloupec je text_clean

+--------------------+-----+--------------------------+--------------------------+
|dialog              |time |text                      |text_clean                |
+--------------------+-----+--------------------------+--------------------------+
|<dialog #1 (2299)>  |07:19|boty do vody              |boty do vody              |
|<dialog #1 (2299)>  |07:19|boty do vody k moři       |boty do vody k mori       |
|<dialog #1 (2299)>  |07:19|boty do vody pro děti     |boty do vody pro deti     |
|<dialog #41205 (15)>|09:43|prodej traktoru zetor 6911|prodej traktoru zetor 6911|
|<dialog #41205 (15)>|10:02|prodej traktoru zetor 6911|prodej traktoru zetor 6911|
|<dialog #41205 (15)>|10:14|prodej traktoru zetor 6711|prodej traktoru zetor 6711|
+--------------------+-----+--------------------------+--------------------------+
```

```
Dále je pak dobré očistit data o tzv. stop words - o slova, která nenesou samy o sobě žádný význam.

V češtině jsou to například slova:
stop_words_czech = [
    "a", "aby", "aj", "ale", "anebo", "ani", "ano", "asi", "aspoň", "atd", "atp",
    "až", "bez", "beze", "blízko", "bohužel", "brzo", "bude", "budeme", "budete",
    "budou", "budu", "by", "byl", "byla", "byli", "bylo", "byly", "bys", "být",...
]
```

```
Tokenizace = rozdělení textu na jednotlivá slova nebo tokeny.
+-------------------------------+
|filtered_words                 |
+-------------------------------+
|[boty, vody]                   |
|[boty, vody, mori]             |
|[boty, vody, deti]             |
|[prodej, traktoru, zetor, 6911]|
|[prodej, traktoru, zetor, 6911]|
|[prodej, traktoru, zetor, 6711]|
+-------------------------------+
```
## Extrakce klíčových slov: 
Důležitou součástí textové analýzy je extrakce klíčových slov nebo frází, které popisují obsah textu. To může být provedeno pomocí technik jako je frekvenční analýza, TF-IDF (Term Frequency-Inverse Document Frequency), nebo použitím metod zpracování přirozeného jazyka (Natural Language Processing - NLP).

```
Pro frekvenční analýzu potřebujeme získat četnost výskytu jednotlivých slov ze vstupního a upraveného datasetu:

+-------------+--------+
| Slovo     | Četnost  |
+-------------+--------+
| boty      | 3        |
| vody      | 3        |
| mori      | 1        |
| deti      | 1        |
| prodej    | 3        |
| traktoru  | 3        |
| zetor     | 3        |
| 6911      | 2        |
| 6711      | 1        |
+-------------+--------+
```

```
Pro výpočet TF-IDF potřebujeme nejprve vytvořit dvě matice. První je Term Frequency matice, která udává jak často se jednotlivá slova vyskytují v každém dokumentu. Druhá matice je Inverse Document Frequency, která udává jak unikátní jsou slova v celém dokumentu...

Tam, kde se vyskytuje 0, tak se nejedná o unikátní slovo v rámci datové sady, tam kde se vyskytuje číslo větší než 0, znamená to, že se jedná o relativně unikátní slovo.

+-------------------------------+----------------------------------------------------+
| filtered_words                | TF-IDF                                             |
+-------------------------------+----------------------------------------------------+
| [boty, vody]                   | [(boty, 0.0), (vody, 0.0), (mori, 0.0), ...       |
| [boty, vody, mori]             | [(boty, 0.0), (vody, 0.0), (mori, 0.602..., ...   |
| [boty, vody, deti]             | [(boty, 0.0), (vody, 0.0), (mori, 0.0), ...       |
| [prodej, traktoru, zetor, 6911]| [(prodej, 0.0), (traktoru, 0.0), (zetor, 0.0), ...|
| [prodej, traktoru, zetor, 6911]| [(prodej, 0.0), (traktoru, 0.0), (zetor, 0.0), ...|
| [prodej, traktoru, zetor, 6711]| [(prodej, 0.0), (traktoru, 0.0), (zetor, 0.0), ...|
+-------------------------------+----------------------------------------------------+

```
```
Použití knihovny NLP - ta sama o sobě provede výše uvedené kroky:
- provede tokenizaci
- odstranění stop slov
- výpočet četnosti slov

Do výsledného dataframu se přidá sloupec keywords (= tokenizace)

+--------------------+-----+--------------------------+--------------------------+-------------------------------+
|dialog              |time |text                      |text_clean                |keywords                       |
+--------------------+-----+--------------------------+--------------------------+-------------------------------+
|<dialog #1 (2299)>  |07:19|boty do vody              |boty do vody              |[boty, vody]                   |
|<dialog #1 (2299)>  |07:19|boty do vody k moři       |boty do vody k mori       |[boty, vody, mori]             |
|<dialog #1 (2299)>  |07:19|boty do vody pro děti     |boty do vody pro deti     |[boty, vody, deti]             |
|<dialog #41205 (15)>|09:43|prodej traktoru zetor 6911|prodej traktoru zetor 6911|[prodej, traktoru, zetor, 6...|
|<dialog #41205 (15)>|10:02|prodej traktoru zetor 6911|prodej traktoru zetor 6911|[prodej, traktoru, zetor, 6...|
|<dialog #41205 (15)>|10:14|prodej traktoru zetor 6711|prodej traktoru zetor 6711|[prodej, traktoru, zetor, 6...|
+--------------------+-----+--------------------------+--------------------------+-------------------------------+

word_frequency (kolikrát se slovo vyskytuje v textu) bude vypadat následovně:

+--------------------------+---------+
|word                      |frequency|
+--------------------------+---------+
|vody                      |3        |
|mori                      |1        |
|deti                      |1        |
|boty                      |3        |
|prodej                    |3        |
|traktoru                  |3        |
|zetor                     |3        |
|6711                      |1        |
|6911                      |2        |
+--------------------------+---------+
```
## Analýza sentimentu: 
Pokud je to relevantní, můžeme provést analýzu sentimentu, která identifikuje a kvantifikuje náladu nebo emocionální náboj v textu (např. pozitivní, negativní, neutrální).
```
Pro analýzu sentimentu v českém jazyce můžeme použít knihovnu **textblob**

+-------------------------------+---------+
|text_clean                     |sentiment|
+-------------------------------+---------+
|boty do vody                   |neutral  |
|boty do vody k moři            |neutral  |
|boty do vody pro děti          |neutral  |
|prodej traktoru zetor 6911     |neutral  |
|prodej traktoru zetor 6911     |neutral  |
|prodej traktoru zetor 6711     |neutral  |
+-------------------------------+---------+

Výsledek není moc dobrý, protože nepoužíváme trénovaný model pro český jazyk a použila jsem pouze jednoduchou metodu knihovny textblob. Pro přesnější výsledky analýzy by bylo potřeba trénovat model na rozsáhlejším a reprezentativním datasetu českých textů s ručně anotovanými sentimenty.
```
## Kategorizace nebo klasifikace: 
V některých případech můžeme provést kategorizaci nebo klasifikaci textu na základě obsahu. Například můžete kategorizovat zprávy jako politické, sportovní, zábavní atd.

```
Nejspíš už nějaká tabulka s kategorizací nebo klasifikací existuje. V tomto případě je potřeba vytvořit trénovací data, tedy data, která obsahují textové vstupy spolu s odpovídajícími kategoriemi.

A samotný postup pro kategorizaci textu pomocí "NLP" může vypadat takto:

- nejprve připravíme trénovací data, rozdelíme je na trénovací a testovací množinu (trénovací data se použijí k natrénovaní klasifikačního modulu a testovací data se použijí na vyhodnocení modelu)
- poté příjde na řadu vektorizace textu: je potřeba převést textová data na vektory, například pomocí TF-IDF (jak je zmíněno výše)
- dalším krokem je natrénování samotného klasifikačního modelu, můžeme použít natrénovaný NLP model (CzechBERT) a trénovací data (můžeme použít metody: logistická regrese, SVM nebo hloubkové neuronové šítě)
- validace modelu - je potřeba ověřit kvalitu natrénovaného modlu pomocí testovacích dat (zajímá nás přesnost, úplnost atd.)
- predikce nových textů - poté, co již máme natrenovaný a ověřený klasifikační model, můžeme použít model k predikci kategorií nebo štítků pro nové zatím neoznačené texty
```
## Vytváření výstupů: 
Výstupy z textové analýzy mohou zahrnovat tabulky, grafy, či vizualizace sentimentu, klíčových slov, nebo kategorií.

## Interpretace výsledků: 
Nakonec je důležité interpretovat výsledky analýzy a vyvodit z nich relevantní závěry nebo akční body.

## **Shrnutí**
Tento celý postup by bylo určitě dobré **zautomazitovat**. Nejprve je potřeba vyřešit **sběr dat**. Jelikož se jedná o velké objemy dat je vhodné použít distribuovaný systém pro sběr dat, který umožňuje získávat data z různých zdojů paralelně a efektivně. Ukládaní dat v data lakes nám umožňuje ukládat nestrukturovaná, strukturovaná i polostrukturovaná data bez nutnosti předběžného definování schématu. Technologie pro distribuované zpracování dat je možné použít Hadoop, Apache Spark nebo jiné distribuované frameworky. Důležitou součástí je také datová kvalita. Je třeba klást velký důraz na datovou kvalitu. Data mohou obsahovat chyby, duplikáty atd. je důležité, aby data byla zvalidována ještě než budou použita k analýze.

Poté, co je vyřešen sběr dat, můžeme nasadit výše uvedé kroky: **predzpracování dat, extrakce klíčových slov, analýza sentimentu, kategorizace atd.** Případně můžeme dle potřeby přidat i další kroky, funkce a postupy, které nebyly výše uvedené.

Na řadu přichází také **optimalizace modelu**, pokud je na místě zlepšit výkon modelu můžeme experimentovat s různými hyperparametry nebo funkcemi.

Dalším krokem je samotné **nasazení modelu**, model můžeme nasadit do produkce, kde bude shopen například klasifikovat nová data.

A posledním krokem je **monitoring a údržba**, je důležité stále monitorovat výkon modelu a pravidelně aktualizovat model, aby zůstal relevantní a přesný. 