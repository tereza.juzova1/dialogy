# Proč podle vás má či nemá smysl použít modifikátory ve vyhledávání k tomu, aby se uživatelé dostali rychleji k informaci, kterou hledají?
## Řešení

Základním principem modifikátorů je upřesnění nebo obohacení klíčových slov. Pomahájí zlepšit relevantnost výsledků. Modifikátory slouží k omezení výsledků na základě specifických kritérií podle určitých charakteristik. 

Můžeme si představit, že uživatel do vyhledavače zadá "dovolená". Modifikátorem pro klíčové slovo **"dovolená"** by mohlo být například: **"levná", "drahá", "letní", "last minute" atd.**

Ve výsledku dotazu budou zahrnuty výsledky, které odpovídají tomuto dotazu. Pokud jsou však použity modifikátory, např.: **"last minute"**, vyhledávací systém upřesní vyhledávání a zobrazí pouze dovolenou last minute.

Tento konkrétní modifikátor pak uživateli pomůže se dostat rychleji k informacím, které zrovna hledá. Modifikátory tak přispívají ke zlepšení uživatelského rozhraní a zvyšují efektivitu.