# Pro každý soubor dat (dialogy1.txt a dialogy2.txt) zjistěte počet všech dialogů

## Řešení
Nejdříve jsem si trochu pohrála se soubory **dialogy1.txt a dialogy2.txt**. Uložila jsem je do složky data/ a poté si je načetla ve Sparku. Chtěla jsem, aby výsledný dataframe měl 3 sloupce: **dialog, time a text**. Mám tedy nyní připraveny dva dataframy, se kterými můžu lépe pracovat. Viz kód níže.

**Pozn.: vycházím z notebooku - [pocet_vsech_dialogu.ipynb](https://gitlab.com/tereza.juzova1/dialogy/-/blob/main/task_development/notebooks/pocet_vsech_dialogu.ipynb?ref_type=heads)**


```
# Import modules
import pyspark
from pyspark.sql import SparkSession
from pyspark.context import SparkContext
from pyspark.sql.functions import col, regexp_extract, when, last, lag, count, countDistinct, collect_list, concat_ws, expr, regexp_replace, monotonically_increasing_id
from pyspark.sql.window import Window
import pyspark.sql.functions as F
import os
```

```
# Vytvoření instance SparkSession
spark = SparkSession.builder.appName("Load and Display Text File").getOrCreate()
```
```
# Cesta ke složce s daty (předpokládáme, že jsou uloženy v adresáři "data")
data_folder = "data"

# Funkce pro načtení a zpracování souboru
def process_file(file_path):
    df = spark.read.text(file_path)

    # Odstranění prázdných řádků
    df = df.filter(df.value != "")

    # Rozdělení hodnot v sloupci "value" do samostatných sloupců pomocí regulárních výrazů
    dialog_pattern = r'<dialog #[0-9]+ \([0-9]+\)>'
    time_pattern = r'^([0-9]{2}:[0-9]{2})'
    text_pattern = r'^[0-9]{2}:[0-9]{2}\s+(.+)'
    df = df.withColumn("dialog", F.regexp_extract(df.value, dialog_pattern, 0))
    df = df.withColumn("time", F.regexp_extract(df.value, time_pattern, 1))
    df = df.withColumn("text", F.regexp_extract(df.value, text_pattern, 1))

    # Vytvoření sloupce "dialog_id" pomocí monotonně rostoucího identifikátoru
    df = df.withColumn("dialog_id", F.monotonically_increasing_id())

    # Nahrazení prázdných hodnot v sloupci "dialog" hodnotami z předchozího neprázdného řádku
    fill_value = F.last("dialog", ignorenulls=True).over(Window.orderBy("dialog_id"))
    df = df.withColumn("dialog", F.when(df.dialog == "", None).otherwise(df.dialog))
    df = df.withColumn("dialog", F.last("dialog", ignorenulls=True).over(Window.orderBy("dialog_id")))

    # Odstranění sloupců "value" a "dialog_id"
    df = df.drop("value", "dialog_id")

    # Odstranění řádků s prázdnými hodnotami ve sloupcích "time" a "text"
    df = df.filter((F.col("time") != "") & (F.col("text") != ""))

    return df

# Seznam souborů v adresáři "data"
file_list = os.listdir(data_folder)

# Filtrujeme pouze soubory končící na ".txt"
txt_files = [file_name for file_name in file_list if file_name.endswith(".txt")]

# Proces načtení a zpracování všech souborů
dataframes = []
for file_name in txt_files:
    file_path = os.path.join(data_folder, file_name)
    df = process_file(file_path)
    dataframes.append(df)

# Zobrazení obsahu DataFrame pro všechny načtené soubory
for idx, df in enumerate(dataframes):
    print(f"DataFrame {idx + 1}:")
    df.show(10)
    #df.show(truncate=False)
```
Takto vypadají výsledné dataframy pro dialogy1 a dialogy2:


```
DataFrame 1:
+------------------+-----+--------------------+
|            dialog| time|                text|
+------------------+-----+--------------------+
|<dialog #1 (2299)>|07:19|        boty do vody|
|<dialog #1 (2299)>|07:19| boty do vody k moři|
|<dialog #1 (2299)>|07:19|boty do vody pro ...|
|<dialog #1 (2299)>|07:20|         boty na beh|
|<dialog #1 (2299)>|07:20|         boty na běh|
|<dialog #1 (2299)>|07:20|       boty na bezky|
|<dialog #1 (2299)>|07:20|       boty na běžky|
|<dialog #1 (2299)>|07:20|boty na běžky výp...|
|<dialog #1 (2299)>|07:20|     boty na házenou|
|<dialog #1 (2299)>|07:20|    boty na nohejbal|
+------------------+-----+--------------------+
only showing top 10 rows
```
```
DataFrame 2:
+-----------------+-----+--------------------+
|           dialog| time|                text|
+-----------------+-----+--------------------+
|<dialog #1 (338)>|02:48|Všechny větrací o...|
|<dialog #1 (338)>|02:50|V přihrádkách na ...|
|<dialog #1 (338)>|02:52|Nepoškoďte potrub...|
|<dialog #1 (338)>|02:56|Při definitivním ...|
|<dialog #1 (338)>|02:56|Při konečném post...|
|<dialog #1 (338)>|02:57|Při konečném post...|
|<dialog #1 (338)>|02:57|Při konečném post...|
|<dialog #1 (338)>|02:58|definitivním post...|
|<dialog #1 (338)>|02:58|definitivním post...|
|<dialog #1 (338)>|03:03|Zařízení je určen...|
+-----------------+-----+--------------------+
only showing top 10 rows
```
Níže uvedený kód nám vrací celkové statistiky pro každý soubor zvlášť. Vždy chceme vědět celkový počet dialogů, celkový počet řádků a počet unikátních hodnot ve sloupci 'text'. Dále pak můžeme vidět výsledky pro statistiku per jednotlivé dialogy, kde sledujeme celkový počet a celkový počet unikátních hodnot.

```
# Výpočet celkových statistik a statistik per jednotlivé dialogy pro každý DataFrame
results_dfs = []
for idx, df in enumerate(dataframes):
    total_rows = df.count()
    total_dialogs = df.select("dialog").distinct().count()
    distinct_texts = df.select("text").distinct().count()

    # Výpočet celkových počtů a unikátních počtů per jednotlivé dialogy
    dialog_counts = df.groupBy("dialog").agg(
        F.count("*").alias("total_count"),
        F.countDistinct("text").alias("unique_count")
    )

    results_df = spark.createDataFrame([
        ("DataFrame", idx + 1),
        ("Celkový počet dialogů", total_dialogs),
        ("Celkový počet řádků", total_rows),
        ("Počet unikátních hodnot ve sloupci 'text'", distinct_texts)
    ], ["Statistika", "Hodnota"])

    results_dfs.append((results_df, dialog_counts))

# Zobrazení celkových statistik a statistik per jednotlivé dialogy pro každý DataFrame
for idx, (results_df, dialog_counts) in enumerate(results_dfs):
    print(f"DataFrame {idx + 1}:")
    print("Celkové statistiky:")
    results_df.show(truncate=False)
    print("Počty per jednotlivé dialogy:")
    dialog_counts.show(truncate=False)
    print("\n")
```
```
DataFrame 1:
Celkové statistiky:
+-----------------------------------------+-------+
|Statistika                               |Hodnota|
+-----------------------------------------+-------+
|DataFrame                                |1      |
|Celkový počet dialogů                    |41205  |
|Celkový počet řádků                      |917590 |
|Počet unikátních hodnot ve sloupci 'text'|422958 |
+-----------------------------------------+-------+
```
```
+------------------+-----------+------------+
|dialog            |total_count|unique_count|
+------------------+-----------+------------+
|<dialog #1 (2299)>|2299       |1670        |
|<dialog #2 (439)> |439        |8           |
|<dialog #3 (338)> |338        |285         |
|<dialog #4 (324)> |324        |85          |
|<dialog #5 (315)> |315        |313         |
|<dialog #6 (298)> |298        |297         |
|<dialog #7 (296)> |296        |3           |
|<dialog #8 (263)> |263        |40          |
|<dialog #9 (263)> |263        |198         |
|<dialog #10 (263)>|263        |59          |
|<dialog #11 (258)>|258        |249         |
|<dialog #12 (187)>|187        |51          |
|<dialog #13 (186)>|186        |180         |
|<dialog #14 (177)>|177        |131         |
|<dialog #15 (173)>|173        |12          |
|<dialog #16 (173)>|173        |72          |
|<dialog #17 (172)>|172        |128         |
|<dialog #18 (172)>|172        |122         |
|<dialog #19 (166)>|166        |152         |
|<dialog #20 (160)>|160        |152         |
+------------------+-----------+------------+
only showing top 20 rows
```
```
DataFrame 2:
Celkové statistiky:
+-----------------------------------------+-------+
|Statistika                               |Hodnota|
+-----------------------------------------+-------+
|DataFrame                                |2      |
|Celkový počet dialogů                    |51621  |
|Celkový počet řádků                      |896754 |
|Počet unikátních hodnot ve sloupci 'text'|463990 |
+-----------------------------------------+-------+
```
```
Počty per jednotlivé dialogy:
+------------------+-----------+------------+
|dialog            |total_count|unique_count|
+------------------+-----------+------------+
|<dialog #1 (338)> |338        |285         |
|<dialog #2 (263)> |263        |59          |
|<dialog #3 (186)> |186        |180         |
|<dialog #4 (173)> |173        |72          |
|<dialog #5 (172)> |172        |128         |
|<dialog #6 (172)> |172        |122         |
|<dialog #7 (166)> |166        |152         |
|<dialog #8 (160)> |160        |152         |
|<dialog #9 (145)> |145        |69          |
|<dialog #10 (127)>|127        |91          |
|<dialog #11 (125)>|125        |66          |
|<dialog #12 (119)>|119        |12          |
|<dialog #13 (112)>|112        |97          |
|<dialog #14 (110)>|110        |86          |
|<dialog #15 (105)>|105        |12          |
|<dialog #16 (104)>|104        |12          |
|<dialog #17 (103)>|103        |98          |
|<dialog #18 (99)> |99         |92          |
|<dialog #19 (99)> |99         |38          |
|<dialog #20 (99)> |99         |55          |
+------------------+-----------+------------+
only showing top 20 rows
```