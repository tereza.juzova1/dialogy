## Analytická úloha

Data v souborech dialogy1.txt. a dialogy2.txt obsahují část dotazů zadaných do vyhledání v rámci jednoho (stejného) dne. Řazené jsou tak, jak šly za sebou v čase. Rozděleny jsou do několika částí - dialogů. Záměrem reálné analytické úlohy bylo zjistit, jak pomocí modifikátorů (viz soubor modifikatory.txt) pomoci uživateli co nejrychleji najít odpověď. Pro zjednodušení použijte modifikátor v přesné shodě.

### Úkoly a otázky:

a) Pro každý soubor dat (dialogy1.txt a dialogy2.txt) zjistěte počet všech dialogů

b) Proč podle vás má či nemá smysl použít modifikátory ve vyhledávání k tomu, aby se uživatelé dostali rychleji k informaci, kterou hledají?

c) Jak můžeme modifikátory ve vyhledávání použít (jak je můžeme nejlépe dostat do stránky s výsledky vyhledávání)?

d) Jak byste z dodaných dat (dialogy1.txt a dialogy2.txt) vytvořil/a tabulku modifikátorů?

e) Záleží podle vás na tom, na jakém místě v dotaze je modifikátor použit?

f) Porovnejte soubor dialogy1.txt a dialogy2.txt a vysvětlete, čím podle vás mohou být rozdíly mezi nimi způsobeny.

g) Co dalšího můžete o dodaných datech říci?

### Soubory

- dialogy1.txt
- dialogy2.txt
- modifikatory.txt